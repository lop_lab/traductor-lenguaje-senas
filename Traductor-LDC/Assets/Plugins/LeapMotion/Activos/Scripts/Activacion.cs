using UnityEngine;
using Leap.Unity;
using System.Collections;

public class Activacion : MonoBehaviour {
    public ExtendedFingerDetector ExtendedFingerDetecto;
    [Header("Finger States")]
    [Tooltip("Required state of the thumb.")]
    public PointingState Thumb = PointingState.Either;

    /** The required index finger state. */

    [Tooltip("Required state of the index finger.")]
    public PointingState Index = PointingState.Either;

    /** The required middle finger state. */

    [Tooltip("Required state of the middle finger.")]
    public PointingState Middle = PointingState.Either;

    /** The required ring finger state. */

    [Tooltip("Required state of the ring finger.")]
    public PointingState Ring = PointingState.Either;

    /** The required pinky finger state. */

    [Tooltip("Required state of the little finger.")]
    public PointingState Pinky = PointingState.Either;

    /** How many fingers must be extended for the detector to activate. */

    [Header("Min and Max Finger Counts")]
    [Range(0, 5)]
    [Tooltip("The minimum number of fingers extended.")]
    public int MinimumExtendedCount = 0;

    /** The most fingers allowed to be extended for the detector to activate. */

    [Range(0, 5)]
    [Tooltip("The maximum number of fingers extended.")]
    public int MaximumExtendedCount = 5;

    // Start is called before the first frame update
    private void Start() {
    }

    // Update is called once per frame
    private void Update() {
        Debug.Log("Dedo  activado");
        StartCoroutine(DedoExtendido());
    }

    IEnumerator DedoExtendido() {
        int required = 0, forbidden = 0;
        PointingState[] stateArray = { Thumb, Index, Middle, Ring, Pinky };
        for (int i = 0; i < stateArray.Length; i++)
        {
            var state = stateArray[i];
            switch (state)
            {
                case PointingState.Extended:
                if (stateArray[0] == PointingState.Extended)
                    Debug.Log("thumb extendido");
                break;

                case PointingState.NotExtended:
                forbidden++;
                break;

                default:
                break;
            }
        }
        yield return null;
    }
}