using System.Collections;
using UnityEngine;

public class Corrutina : MonoBehaviour {
    private Vector3 m1 = new Vector3(-1, 0, 0);
    private Vector3 m2 = new Vector3(1, 0, 0);
    private Vector3 s1 = new Vector3(1, 1, 1);
    private Vector3 s2 = new Vector3(0.5f, 0.5f, 0.5f);

    private void Start() {
        StartCoroutine(DoStuff());
    }

    private IEnumerator DoStuff() {
        while (true)
        {
            switch (UnityEngine.Random.Range(0, 2))
            {
                case 0:
                yield return StartCoroutine(Move());
                break;

                case 1:
                yield return StartCoroutine(Scale());
                break;
            }
        }
    }

    private IEnumerator Move() {
        Vector3 target = transform.position == m1 ? m2 : m1;
        while (transform.position != target)
        {
            yield return new WaitForSeconds(0);
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime);
        }
    }

    private IEnumerator Scale() {
        Vector3 target = transform.localScale == s1 ? s2 : s1;
        while (transform.localScale != target)
        {
            yield return null;
            transform.localScale = Vector3.MoveTowards(transform.localScale, target, Time.deltaTime);
        }
    }
}