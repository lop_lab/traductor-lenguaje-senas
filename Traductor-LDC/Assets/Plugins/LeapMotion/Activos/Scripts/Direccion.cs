﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Direccion : MonoBehaviour
{
   public Text direccion;

    public void MostrarDireccion(string text)
   {
      direccion.text = text;
   }
}
