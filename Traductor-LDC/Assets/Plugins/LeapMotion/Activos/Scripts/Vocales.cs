﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vocales : MonoBehaviour
{
	private void Start()
	{
	}

   public Text textoVocal;
   public GameObject vocalA;
   public GameObject vocalE;
   public GameObject vocalI;
   public GameObject vocalO;
   public GameObject vocalU;
   public GameObject Particulas;
   Material m;
   float y;
   bool particulaActiva = false;
   GameObject p;
   public void Texto(string t)
   {
      textoVocal.text = t;
   }
   public void MostrarVocal(char text)
   {
      // Instantiate(letraA, transform.position, new Quaternion(90f,180f,0f,1f));
      //textoVocal.text = text.ToString();      
      switch (text)
      {
         case 'O':
               RotarVocal(vocalO);
            textoVocal.text = "Vocal O";
            break;
         case 'A':
               RotarVocal(vocalA);
            textoVocal.text = "Vocal A";
            break;
         case 'I':
               RotarVocal(vocalI);
            textoVocal.text = "Vocal I";
            break;
         case 'U':
            RotarVocal(vocalU);
            textoVocal.text = "Vocal U";
            break;
         case 'N':
            DesactivarVocal();
            textoVocal.text = "*******";
            break;
      }
   }

  public void CompararAngulo(Vector3 a, Vector3 b)
   {
   }

   float rotar;
   
   public void RotarVocal(GameObject vocal)
   {
        
      vocal.SetActive(true);
      y += Time.deltaTime * 100;
      vocal.transform.rotation = Quaternion.Euler(0, y, 0);
      p = Instantiate(Particulas, vocal.transform.position, Quaternion.identity);      
      Destroy(p, 5f);
   }
   void DesactivarVocal()
   {
      vocalO.SetActive(false);
      vocalO.transform.rotation = Quaternion.Euler(0, 160, 0);
      vocalA.SetActive(false);
      vocalA.transform.rotation = Quaternion.Euler(0, 160, 0);
      vocalI.SetActive(false);
      vocalI.transform.rotation = Quaternion.Euler(0, 150, 0);
      vocalU.SetActive(false);
      vocalU.transform.rotation = Quaternion.Euler(0, 150, 0);
   }

}
