﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Subirnumero : MonoBehaviour {
    public Text txtNum;
    public GameObject uno;
    public GameObject dos;
    public GameObject tres;
    public GameObject cuatro;
    public GameObject cinco;
    public GameObject seis;
    public GameObject siete;
    public GameObject ocho;
    public GameObject nueve;
    public GameObject cero;
    public Text conteo;

    private Color[] colors = new Color[] { Color.green, Color.cyan, Color.red, Color.blue };
    private IEnumerator Comenzarconteo(){

        yield return null;
    }
    private IEnumerator CambiarColor(GameObject num) {
        int i = 0;

        while (true)
        {
            num.GetComponent<MeshRenderer>().material.color = colors[i];
            i++;

            if (i == colors.Length)
            {
                i = 0;
            }

            // Pausamos el método durante dos segundos
            yield return new WaitForSeconds(2f);
        }
    }

    private void Start() {
    }

    private void FixedUpdate() {
    }

    public void Subiendo(int numero) {

        switch (numero)
        {
            case 0:
            Subir(cero);
            break;

            case 1:
            Subir(uno);
            break;

            case 2:
            Subir(dos);
            break;

            case 3:
            Subir(tres);
            break;

            case 4:
            Subir(cuatro);
            break;

            case 5:
            Subir(cinco);
            break;

            case 6:
            Subir(seis);
            break;

            case 7:
            Subir(siete);
            break;

            case 8:
            Subir(ocho);
            break;

            case 9:
            Subir(nueve);
            break;
        }
    }

    private void Subir(GameObject numero) {
        int r = Random.Range(0, 4);
        txtNum.text = "NUMERO - " + numero.name.Substring(6) + " -";
        numero.transform.Translate(Vector3.up * Time.deltaTime * 5);
        numero.transform.Translate(Vector3.back * Time.deltaTime * Random.Range(0, 0.5f));
        numero.transform.Translate(Vector3.forward * Time.deltaTime * Random.Range(0, 0.5f));
        numero.transform.rotation = Quaternion.Euler(0, 180, 0);
        StartCoroutine(CambiarColor(numero));

        //numero.GetComponent<Renderer>().material.color = colors[r];
    }

    public void Retornar() {
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
        gameObject.GetComponent<Renderer>().material.color = Color.white;
        StopCoroutine(CambiarColor(gameObject));
    }
}